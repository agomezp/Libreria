﻿var app = angular.module('myapp', []);

app.controller('Ctrl', function ($scope) {
    $scope.titulo = '';
    $scope.categoria = '';
    $scope.tituloLibro = '';
    $scope.autorLibro = '';
    $scope.formatoLibro = '';
    $scope.idiomaLibro = '';
    $scope.editorialLibro = '';
    $scope.categoriaLibro = '';
    $scope.Ejemplar = '';
    $scope.Idioma = '';
    $scope.Direccion = '';
    $scope.Formato = '';
    $scope.Autor = '';
    $scope.Editorial = '';

    $scope.InsertEditorial = function () {

        $http({
            method: 'POST',
            url: '/Agregar/InsertEditorial?editorial=' + $scope.Editorial
        }).then(function () {
            alert('insertado correctamente');
        }, function () {
            alert('Error')
        });
    };

});