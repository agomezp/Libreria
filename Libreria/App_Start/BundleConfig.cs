﻿using System.Web;
using System.Web.Optimization;

namespace Libreria
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/angular.min.js",
                      "~/Scripts/angular.js",
                      "~/Scripts/controlador.js",
                      "~/Scripts/respond.js",
                      "~/Scripts/jquery.min.js",
                      "~/Scripts/._jquery.min.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/._default.css",
                      "~/Content/._pandoc-code-highlight.css",
                      "~/Content/default.css",
                      "~/Content/pandoc-code-highlight.css",
                      "~/Content/semantic.min.css",
                      "~/Content/site.css"));

            //bundles.Add(new ScriptBundle("~/Scripts/js").Include(
            //          "~/Scripts/angular.js",
            //          "~/Scripts/._jquery.min.js",
            //          "~/Scripts/jquery.min.js",
            //          "~/Scripts/angular.min.js"));
                
        }
    }
}
